package com.peepalsoft.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class PeepalsoftEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PeepalsoftEurekaApplication.class, args);
    }

}
